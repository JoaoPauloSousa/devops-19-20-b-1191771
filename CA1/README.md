# README #

### **1- Analysis, Design and Implementation**
nota: Considerei que este trabalho é continuação do anterior, pelo que deixei o campo _jobTitle_ no projecto.

**1º Tag the initial version as v1.2.0**

- Para _tagar_ a versão inical como pretendido usamos o comando: 

        git tag v1.2.0

- No meu caso cometi um erro "ortográfico" e criei a tag v.1.2.0, pelo que tive de criar uma nova tag chamada v1.2.0 e apagar a antiga usando o comando:

          git tag -d v.1.2.0

-No entanto como me apercebi mais tarde isto não é suficiente pois só apaga localmente, sendo necessário realizar o push correcto para que a alteração surta efeitos no bitbucket (por exemplo), sendo assim após usar o comando anterior temos também de usar este:

    git push origin :refs/tags/v.1.2.0


**2º You should create a branch called _email-field_**

- De seguida para criar a nova branch chamada *email-field*, utilizando o comando: 

        git checkout -b email-field 

- Este comando cria um novo ramo e muda para esse mesmo ramo.

- Alternativamente poderíamos optar por uma solução de dois passos, primeiro criar a branch e só depois mudar para ela, usado este dois comandos:
 
        git branch email-field
        
        git checkout email-field

**3º You should add support for email field**

- Nesta fase tivemos de fazer algumas alterações ao repositório 

- Em primeiro lugar na classe **_Employee_** tivemos de adicionar um novo atributo _email_ do tipo _String_ ao construtor;
- Após termos feito as alterações na classe **_Employee_**  vamos à classe **_DataBaseLoader_** e no método _public void run_ adicionamos o novo _string_ do email;
- Por último temos de ir ao ficheiro **_app.js_** e efectuar alterações em todas as entradas necessárias. Neste caso serão duas nomeadamente: 

        <th>Email</th>
   
        <td>{this.props.employee.email}</td>

 **4º You should also add unit tests for testing the creation of Employees and the validation of its attributes 
(for instance, no null/empty values)**

- Na classe Employee, nos métodos Set respectivos de cada atributo, adicionamos restrições para garantir que não permitimos dados inválidos como _null_, _strings_ em branco, _strings_ só com espaços, etc, caso os dados não sejam válidos são lançadas excepções. 

- De seguida criamos a  respectiva classe de testes e fazer a devida validação dos métodos _set_ testando todas as situações possíveis.

**5º You should debug the server and client parts of the solution**

- De forma a realizar este _debug_ devemos utilizar o comando: 

    git bisect

**6º When the new feature is completed (and tested) the code should be merged with the master and a new 
tag should be created (e.g, v1.3.0)**

- Nesta fase, após realizarmos os testes necessários acedemos ao terminal do _IntelliJ_ ou linha de comandos do Windows e usamos os seguintes comandos:

-Para adicionar ficheiros ao _Stage_ usamos:
    
    git add "nomeDoFicheiro"

-Em alternativa se desejarmos adicionar tudo podemos usar:

    git add

-De seguida realizamos o _commit_ usando o comando (neste caso vou colocar mensagem):

    git commit -m “mensagemAIntroduzir”

-É importante referir nesta altura que existem outras alternativas. Eu por exemplo usei o comando:

    git commit -a -m "mensagemAIntroduzir"

Desta forma além de adicionar todos os ficheiros alterados em _Stage_, também realizei o _commit_, usando apenas um comando.

-Após este passo fazemos _push_ através do comando:

    git push

-De seguida vamos realizar o merge com o _master_, sendo assim necessitamos primeiro de mudar para o ramo _master_ usando o comando:

    git checkout -b master

-Só então usamos o comando: 

    git merge "nome do branch que queremos juntar" (neste caso chamado email-field)

-Por último temos que nomear esta versão com o nome v1.3.0, e sendo assim usamos o comando: 

    git tag "nome da versão"(neste caso v1.3.0)


**7º Create a branch called fix-invalid-email. The server should only accept Employees with a valid email 
(e.g., an email must have the "@" sign)**

- Aqui pretende-se criar um novo ramo(_branch_) de forma a adicionar mais validações ao campo e-mail portanto vamos começar por criar outro ramo usando o comando: 

        git checkout -b "nome do ramo" (neste caso será fix-invalid-email)
        
- Este comando em específico cria um novo _branch_ e muda automaticamente para esse ramo criado.

- De seguida vamos à classe *_Employee_* e adicionamos ao método setEmail a condição que verifica que contém o @, mantendo as condições anteriores; 
            
        public void setEmailField(String emailField) {
           if ((emailField !=null && emailField.contains("@"))==true){
               this.emailField = emailField;
           }else {
               throw new IllegalArgumentException("Invalid email");
           }
       }
}
**8º You should debug the server and client parts of the solution.**

- Utilizar comando: 

        git bisect

**9º When the fix is completed (and tested) the code should be merged into master and a new tag should be created 
(with a change in the minor number, e.g., v1.3.0 -> v1.3.1)**

- Nesta fase vou apenas recapitular pois os comandos pois são em tudo idênticos aos anteriores. Têm de ser realizados nesta sequência:

        git add "nomeFicheiro" . 
        git commit -m “nomeDoCommit”;
        git push
        git checkout -b master;
        git merge "nomeDoBranch";
        git tag v1.3.1;

**10º At the end of the assignment mark your repository with the tag ca1.**
- Utilizar este comando já após fazer o push com o readMe: 

        git tag ca1.
        
- No entanto esta _tag_ não irá ser mostrada remotamente até ser efectuado o comando:

        git push origin ca1


### **2- Alternative Solution**

- Após alguma pesquisa sobre alternativas decidi apresentar o _SVN (Subversion)_ como alternativa por não ser um DVCS (sistema de controlo de versões distribuído) como o GIT, podendo assim explicar algumas diferenças entre os dois sistemas.

- O _SVN_ é o mais popular dos CVCS (sistema de controlo de versões centralizado), e até 2014 foi mesmo o mais utilizado considerando ambos os sistemas, sendo na altura ultrapassado pelo GIT.
- Devido ao _SVN_ ser um CVCS, todos os desenvolvedores de software acedem a um único repositório centralizado, e isso significa que quando uma alteração é feita, esta é "passada" a todos os desenvolvedores de software antes que estes possam realizar os seus commits.
- Outra característica é que embora não sendo impossível trabalhar offline, fazê-lo é extremamente difícil pois só existe um único "verdadeiro" repositório, ao contrário dos sistemas DVCS em que todo o repositório está presente nos pc's dos desenvolvedores de software e estes podem trabalhar mesmo com conecções de internet fracas ou inexistentes, sendo apenas necessário conexão à internet quando se realiza os _pull_ e _push_.
- O trabalho em _SVN_ consiste em três partes, _trunk_, _branches_ e _tags_.

1- _trunk_ é o ponto central do produto actual, estável, que inclui os testes e código 100% funcional.

2- _branches_ é onde se pode introduzir e testar novo código, funcionalidades, etc. É uma cópia do _trunk_ onde cada membro da equipa pode fazer alterações sem interferir com o trabalho dos colegas.

3- _tags_ é considerada uma cópia da _branch_ num dado ponto do tempo. Não são usadas durante o desenvolvimento mas sim após o deploy da _branch_ quando esta é terminada. Efectuando esta marcação é mais fácil efectuar a revisão, e, se necessário, reverter no futuro o código para um determinado ponto.

- O modo de funcionamento usual é o seguinte. Quando se quer criar uma nova funcionalidade cria-se um novo ramo do _trunk_ que se coloca numa nova pasta dedicada a armazenar os vários ramos, desenvolve-se a nova funcionalidade nesse ramo e após a conclusão do trabalho efectua-se um _merge_ com o _trunk_.
- Um dos problemas com esta abordagem é que se um desenvolvedor efectua mal o _merge_ ou introduz código com erros no _trunk_, pode "destruir" as _builds_ de todos os outros desenvolvedores, além disso a versão a ser fundidada com a de _trunk_ não reflete as mais recentes alterações que os outros desenvolvedores estão a efectuar nos seus próprios _branches_.
- No entanto este processo é um pouco mais simples que o usado em GIT e requer uma aprendizagem inicial muito curta pelo que continua a ser muito adoptado (embora cada vez menos). A "história" do desenvolvimento de versões também é mais simples do que a de GIT.
- Outras das diferenças mais importantes é o nível de acesso que cada desenvolvedor tem. Em _Git_, por defeito, todos os desenvolvedores possuem o mesmo nível de acesso, já em _SVN_ cada desenvlvedor pode ter direitos de escrita/leitura por nível de ficheiros e/ou por nível de pastas.
- Existem mais algumas diferenças mas são diferenças menos importantes pelo que não vale a pena enumerá-las. De forma geral _Git_ está a ter cada vez mais adopção e o _SVN_ tem vindo a perder utilizadores.

Segue-se abaixo uma tabela com várias diferenças entre alguns dos comandos:

| Git 	| Operation 	| Subversion |
| ------------- |:-------------:| -----:|
| `git clone` 	| Copy a repository	| `svn checkout` |
| `git commit` 	| Record changes to file history 	| `svn commit` |
| `git show` 	| View commit details 	| `svn cat` |
| `git status` 	| Confirm status 	| `svn status` |
| `git diff` 	| Check differences 	| `svn diff` |
| `git log` 	| Check log 	| `svn log` |
| `git add` 	| Addition 	| `svn add` |
| `git mv` 	    | Move 	| `svn mv` |
| `git rm` 	    | Delete 	| `svn rm` |
| `git reset`   | Cancel change 	| `svn revert1` |
| `git branch`   | Make a branch 	| `svn copy2` |
| `git checkout` 	| Switch branch 	| `svn switch` |
| `git merge` 	| Merge 	| `svn merge`
| `git tag` 	| Create a tag 	| `svn copy2`
| `git pull` 	| Update 	| `svn update` |
| `git fetch` 	| Update 	| `svn update` |
| `git push` 	| It is reflected on the remote 	| `svn commit3` |
| `gitignore` 	| Ignore file list 	| `.svnignore` |

- De seguida vamos resumir o que seria uma possível implementação em _SVN_ do exercício proposto, os comandos que colocarei são exemplificativos sendo que os nomes utilizados teriam de ser, naturalmente, adaptados ao nosso projecto.

- Devido à natureza centralizada do _SVN_ na realidade criar uma _branch_ ou um _tag_ tem o mesmo efeito prático portanto podemos começar por efectuar uma cópia do projecto através do comando:

        svn copy http://example.com/svn/trunk http://example.com/svn/tags/name

- Desta forma foi criada uma tag que deverá ser colocada num directório específico para tags.
- De seguida podemos criar uma nova _branch_ e a maneira de o fazer é com os comandos:
    
        svn copy http://example.com/svn/trunk http://example.com/svn/branches/branch
        svn switch http://example.com/svn/branches/branch 
        
- O primeiro comando cria a nova _branch_ e o segundo faz a mudança para o _branch_ criado.
- Após as necessárias mudanças no código e após este ser testado com sucesso iríamos realizar o merge usando o comando:

        (assuming the branch was created in revision 20 and you are inside a working copy of trunk)
        svn merge -r 20:HEAD http://example.com/svn/branches/branch
        
- Nesta fase faríamos novamente uma _tag_ com o comando descrito anteriormente
 
        svn copy http://example.com/svn/trunk http://example.com/svn/tags/name 
        
 - Os próximos passos são repetições dos anteriores pelo que não vale a pena repetir. Resumidamente seria criada nova branch, realizar nela as taregas do fix do e-mail, realizar o merge com o _trunk_ e criar uma nova tag.
 - Após isto faríamos o relatório no readMe, criaríamos uma nova tag e encerrava-se o trabalho.

-Sites de apoio usados para obter informações sobre a alternativa.

https://medium.com/@MentorMate/svn-git-out-of-here-how-to-choose-your-version-control-system-10b44750a75c

https://backlog.com/blog/git-vs-svn-version-control-system/

https://www.perforce.com/blog/vcs/git-vs-svn-what-difference

http://git.or.cz/course/svn.html


