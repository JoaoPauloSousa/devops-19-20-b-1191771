/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String jobTitle;
    private String emailField;

    public Employee() {
    }

    public Employee(String firstName, String lastName, String description, String jobTitle, String emailField) {
       setFirstName(firstName);
        setLastName(lastName);
        setDescription(description);
        setJobTitle(jobTitle);
        setEmailField(emailField);

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
            this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if ((isStringValid(firstName))){
            this.firstName = firstName;
        } else {
            throw new IllegalArgumentException("Invalid First Name");
        }
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        if ((isStringValid(lastName)) ==true) {
            this.lastName = lastName;
        }else {
            throw new IllegalArgumentException("Invalid Last Name");
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if ((isStringValid(description)) == true) {
            this.description = description;
        }else {
            throw new IllegalArgumentException("Invalid Description");
        }
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        if ((isStringValid(jobTitle)) == true) {
        this.jobTitle = jobTitle;
        }else {
            throw new IllegalArgumentException("Invalid Job Title");
        }
    }

    public String getEmailField() {
        return emailField;
    }

    public void setEmailField(String emailField) {
        if ((emailField !=null && emailField.contains("@"))==true){
            this.emailField = emailField;
        }else {
            throw new IllegalArgumentException("Invalid email");
        }
    }

    public boolean isStringValid (String string){
        if (string !=null && !string.isEmpty() && !string.trim().isEmpty()){
            return true;
        }
        return false;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", emailField='" + emailField + '\'' +
                '}';
    }

    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof Employee)) return false;
        if (!super.equals(object)) return false;
        Employee employee = (Employee) object;
        return java.util.Objects.equals(id, employee.id) &&
                java.util.Objects.equals(firstName, employee.firstName) &&
                java.util.Objects.equals(lastName, employee.lastName) &&
                java.util.Objects.equals(description, employee.description) &&
                java.util.Objects.equals(jobTitle, employee.jobTitle) &&
                java.util.Objects.equals(emailField, employee.emailField);
    }

    public int hashCode() {
        return Objects.hash(super.hashCode(), id, firstName, lastName, description, jobTitle, emailField);
    }
}
// end::code[]
