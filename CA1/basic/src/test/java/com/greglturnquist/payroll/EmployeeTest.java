package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    /**
     * Test for constructor - Happy Case
     */
    @Test
    @DisplayName("Test for constructor - Happy Case")
    void employeeConstructorTestHappyCase() {
        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");

        assertTrue(joao instanceof Employee);
    }

    /**
     * Test of null firstName -  firstName cannot be null
     */
    @Test
    @DisplayName("Test of null FirstName -  First name cannot be null")
    void nullFirstName() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setFirstName(null);
        });
    }

    /**
     * Test of empty firstName -  firstName cannot be empty
     */
    @Test
    @DisplayName("Test of null FirstName -  First name cannot be empty")
    void emptyFirstName() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setFirstName("");
        });
    }

    /**
     * Test of only spaces lastName -  lastName cannot be only spaces
     */
    @Test
    @DisplayName("Test of only spaces firstName -  First name cannot be only spaces")
    void onlySpacesFirstName() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setFirstName("   ");
        });
    }

    /**
     * Test of null lastName -  lastName cannot be null
     */
    @Test
    @DisplayName("Test of null lastName -  Last name cannot be null")
    void nullLastName() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setLastName(null);
        });
    }

    /**
     * Test of empty lastName -  lastName cannot be empty
     */
    @Test
    @DisplayName("Test of empy lastName -  Last name cannot be empty")
    void emptyLastName() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setLastName("");
        });
    }

    /**
     * Test of only spaces lastName -  lastName cannot be only spaces
     */
    @Test
    @DisplayName("Test of only spaces lastName -  Last name cannot be only spaces")
    void onlySpacesLastName() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setLastName("   ");
        });
    }

    /**
     * Test of null Description -  Description cannot be null
     */
    @Test
    @DisplayName("Test of null Description -  Description cannot be null")
    void nullDescription() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setDescription(null);
        });
    }

    /**
     * Test of empty Description -  Description cannot be empty
     */
    @Test
    @DisplayName("Test of null Description -  Description cannot be empty")
    void emptyDescription() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setDescription("");
        });
    }

    /**
     * Test of only spaces Description -  Description cannot be only spaces
     */
    @Test
    @DisplayName("Test of only spaces Description -  Description cannot be only spaces")
    void onlySpacesDescription() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setDescription("   ");
        });
    }

    /**
     * Test of null jobTitle -  Job title cannot be null
     */
    @Test
    @DisplayName("Test of null jobTitle -  Job Title cannot be null")
    void nullJobTitle() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setJobTitle(null);
        });
    }

    /**
     * Test of empty Job Title -  Job Title cannot be empty
     */
    @Test
    @DisplayName("Test of null Job Title -  Job Title cannot be empty")
    void emptyJobTitle() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setJobTitle("");
        });
    }

    /**
     * Test of only spaces Job Title -  Job Title cannot be only spaces
     */
    @Test
    @DisplayName("Test of only spaces Job Title -  Job Title cannot be only spaces")
    void onlySpacesJobTitle() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setJobTitle("   ");
        });
    }

    /**
     * Test of null Email -  Email field cannot be null
     */
    @Test
    @DisplayName("Set email - happy case")
    void happyCaseEmail() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");

        joao.setEmailField("baddentist@gmail.com");
        String result = joao.getEmailField();
        String expected = "baddentist@gmail.com";

        assertEquals(result, expected);
    }

    /**
     * Test of null Email -  Email field cannot be null
     */
    @Test
    @DisplayName("Test of null Email -  Email field cannot be null")
    void nullEmail() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setEmailField(null);
        });
    }

    /**
     * Test of empty Email -  Email field cannot be empty
     */
    @Test
    @DisplayName("Test of null Email -  Email field cannot be empty")
    void emptyEmail() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setEmailField("");
        });
    }

    /**
     * Test of only spaces Email -  Email field cannot be only spaces
     */
    @Test
    @DisplayName("Test of only spaces Email -  Email field cannot be only spaces")
    void onlySpacesEmail() {

        Employee joao = new Employee("João", "Sousa", "The extractor", "Dentist", "baddentist@gmail.com");
        assertThrows(IllegalArgumentException.class, () -> {
            joao.setEmailField("   ");
        });
    }
}