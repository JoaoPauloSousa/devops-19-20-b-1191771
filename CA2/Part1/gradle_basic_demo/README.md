CA2, Part 1: DescriptionOfApp / Goals
===================

- This is a demo application that implements a basic multithreaded chat room server.

- The server supports several simultaneous clients through multithreading. When a client connects the server requests a screen name, and keeps requesting a name until a unique one is received. After a client submits a unique name, the server acknowledges it. Then all messages from that client will be broadcast to all other clients that have submitted a unique screen name. A simple "chat protocol" is used for managing a user's registration/leaving and message broadcast.

- The goal of the Part 1 of this assignment is to practice gradle using a very simple example.

**1- You should start by downloading and commit to your repository (in a folder for Part
 1 of CA2) the example application available at
 https://bitbucket.org/luisnogueira/gradle_basic_demo/. You will be
 working with this example**

- This assignment (part 1) is based on a _Gradle basic demo_ project, so we must start from downloaded that project from this link.

      https://bitbucket.org/luisnogueira/gradle_basic_demo/
        
 - After that we can uploaded the project to our individual repository committing them using the code:
 
       git commit -A -m "<Initial project>"
       git push
        
-Note: I start work immediately and skip this step without notice it at the time.
  
**2- Read the instructions available in the readme.md file and experiment with the
application**
-----

- To generate a .jar file with the project we need to open the terminal and run the command:

      gradlew build 
    
- This command will generate a _build_ folder to the project with all the project's files: classes, distributions, resources, scripts, temporary files. 
- It will also generated a zipped file _.jar_ in the _build/libs_ folder with all this information.

**3- Run the server**

- After that we must open the terminal on Windows or IntelliJ itself and execute the following command on the project's root directory:

      java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

**4 -Run a client**

- Open another terminal (Windows or IntelliJ itself) and execute the following gradle task from the project's root directory:

      gradlew runClient

- Note: The above task assumes the chat server's IP is "localhost" and its port is "59001".

- To run several clients, you just need to open more terminals on Windows or IntelliJ and repeat the invocation of the runClient gradle task.
- After the successful build message, a chat window is supposed to be opened and then we can introduce our name and use it.

**5- Add a new task to execute the server**

 - So, after we successful test the app we need to add a task similar to the _runClient task_ in the build.gradle file. In my case i called it _runServer_ and it have the fowling code:
 
        task runServer(type: JavaExec, dependsOn: classes) {
        classpath = sourceSets.main.runtimeClasspath
 
        main = 'basic_demo.ChatServerApp'
 
        args '59001'
        }
        
- In order to check if this code is correct, we have two options:

Option 1 - Open the terminal and show the list of all available tasks running the code:

    gradlew tasks --all
   
Option 2 - Open the terminal and run the server by executing the task (running the code):
 
    gradlew executeServer
    
 -Note: At this moment i did a commit (5cabe7a) and push.
 
 **6- Add a simple unit test and update the gradle script so that it is able to execute the test.** 
 
 - At this moment we need to create a test class of the _App_ class and copy the given code and imports to it, the path should look like this _"src/test/java/basic_demo/AppTest.java"_ 
 The code and imports given in the pdfs are the following:
 
        package basic_demo;
        import org.junit.Test;
        import static org.junit.Assert.*;
        public class AppTest {
        @Test public void testAppHasAGreeting() {
        App classUnderTest = new App();
        assertNotNull("app should have a greeting", classUnderTest.getGreeting());
        }
        }
  
-Note: We don't need to necessarily copy the imports manually.

- The unit tests require junit 4.12 to execute so we need to match it hading this line right below the two _Apache Log4J_ dependencies on build.gradle or download it directly from the compiler program (if it is capable of doing it).

      testCompile group: 'junit', name: 'junit', version: '4.12'
      
-Note: At this time i did a commit(0371858) and push.

**7- Add a new task of type Copy to be used to make a backup of the sources of the application.**

- In order to create a backup of the src (source) of the application a _Copy task_ has been added to _build.gradle_.
- The necessary code to do this task is:

      task backup (type: Copy){
      from 'src'
      into 'backup'
      }
      
- When executed this _task_ copies all files in _src_ (main, test and all subsequent folders and files) to a folder named _backup_.

**8- Add a new task of type Copy to be used to make a backup of the sources of the application.**

- The next thing to do is to build a new _task_ of type _Zip_ and had it to _build.gradle_ in order to copy the contents of the _src_ (source) folder to a new folder which will be called _zipfiles_:
- The necessary code to do this task is:

      task zipFile (type:Zip) {
      from 'src'
      archiveFileName = 'src.zip'
      destinationDirectory = file('zipfiles')
        } 

- When executed this _task_ just create a copy and put all of _src_folders and files into a _zip_ file.

**9- At the end of the part 1 of this assignment mark your repository with the tag ca2-part1**

-Note: At this time i did a commit(2967367) and push.

- Last but not least we need to create a tag to mark our implementation at this point on time. In order to do that we just open the terminal and write the code in this order:

        git tag ca2-part1
        git push origin ca2-part1

- But remember, we only tag AFTER commit and push the code.

 - Since i forgot to include the final version of the readme before the commit, push and tag...i decided to delete the tag, commit (7867c35) "Send final readme", push it and then create the same tag again. I'm happy to say that by now i consider it a very simple process. 



     

 
 
 