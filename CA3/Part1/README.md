CA3, Part 1: Goals/Requirements
===================

- The goal of the Part 1 of this assignment is to practice with VirtualBox using the same projects from the previous assignments but now inside a VirtualBox VM with Ubuntu

**1- You should start by creating your VM as described in the lecture "Introduction to Virtualization" with the following requirements.**

* Minimal installation media of ***Ubuntu 18.04***. 
* **_2048 MB_** RAM
* **_10 GB_** storage
* Network Adapter 1 as ***NAT***
* Network Adapter 2 as ***Host-only Adapter (vboxnet0)***, with IPv4 Address **_192.168.56.1_**

##### Setup VM
After creating the VM, the next step was to configure its network, also following the lecture slides:
- Install the network tools with:

        sudo apt install net-tools
    
- Launch network configuration ﬁle with:

        sudo nano /etc/netplan/01-netcfg.yaml

- Edit the file to setup the IP as:
    
        network:
           version: 2
           renderer: networkd
           ethernets:
               enp0s3:
                dhcp4: yes
               enp0s8:
                addresses:
                    - 192.168.56.5/24

- Apply the new changes with:

        sudo netplan apply
        
- Install openssh-server with:

        sudo apt install openssh-server
 
- Launch ssh configuration file with:

        sudo nano /etc/ssh/sshd_conﬁg
        
- Enable password authentication for ssh with by uncomment the line:

        PasswordAuthentication yes

- Restart ssh service with:

        sudo service ssh restart
        
##### Access VM with ssh
- Since the SSH server is enabled in the VM we can now use ssh to connect to the VM:
- In order to do this we must type in the host (Windows or Linux or OSX), in a terminal/console:

      ssh joaosousa@192.168.56.5
        
- Note that _joaosousa_ should be replaced by the user name of the VM and _192.168.56.5_ should be replaced by the IP of the VM     
      
**2- You should clone your individual repository inside the VM**

##### Install the necessary packages/tools to run the project. Clone the project.

- Now that we can access VM through a console is easier to install the necessary tools (just the fact that _copy paste_ work in the terminal is already a bonus).
- In order to install the necessary just copy the following commands in terminal:

      sudo apt install git
      sudo apt install openjdk-8-jdk-headless
      sudo apt install maven
      sudo apt install gradle 
     
- After the instalation now we can clone the repository (now we already have git) by using the following command _git clone_ with the repository link, in my case:
 
      git clone https://JoaoPauloSousa@bitbucket.org/JoaoPauloSousa/devops-19-20-b-1191771.git      

**3- 3 You should try to build and execute the spring boot tutorial basic project and the gradle_basic_demo project (from the previous assignments)**
  
- Since _spring boot tutorial basic project_ is the exercise CA1 we first start to run the project by going to the _basic_ folder inside CA1 in the terminal, in my case doing:
 
      cd devops-19-20-b-1191771/CA1/basic 
      
- Give the necessary execution permissions to maven by doing:

      chmod +x mvnw
 
- And then execute the command:

      ./mvnw spring-boot:run
      
- If everything went well now we can open the application in going to the webrowser and put the following link:

      http://192.168.56.5:8080

- We can now obtain the confirmation that everything is right because the table shows as intended.

- Ok, the first part is concluded, but we need to run the _gradle_basic_demo project_ too, so now we must start by navigating to the _gradle_basic_demo_ folder inside part 1 of CA2, in my case doing:

      cd devops-19-20-b-1090118/CA2/Part1/gradle_basic_demo
    
- Then give the necessary execution permissions to the gradle executable by doing:    

      chmod +x gradlew 
      
- And then execute the command to build:
 
      ./gradlew build     
  
- And finally run the server, using the following command:

      java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
      
- Now all we need before run the client is to change the ip in the host machine, so we need to go to _build.gradle_ which is inside part 1 of CA2, and in the task _runClient_ make it to loke something like that:
          
       task runClient(...){
		       (...)
		       args '192.168.56.5', '59001'
	    }
	
- After that change i create a issue and did a commit called "fix #14: Change task runClient in order to work with VM". After that i push the changes to the repository. 	      
	
- Now in the host machine (like IntelliJ terminal for example) we need to navigate too to the _gradle_basic_demo_ again (same steps as before) and introduce the following command:
	
	    gradlew runClient 
	
- If everything went well the chat App should be launched.

- In this step i add to .gitignore the _node_ and _node_modules_ and go back to.

      cd devops-19-20-b-1090118/CA2/part2/demo

- And in the IntelliJ terminal (just because it was easier and i was already there) introduce the following commands (i didn't have the _node_modules_ so i just did it for the _node_):

      sudo rm -rf node

- After that i introuduce the folowwing commands to push the changes to the repository in order to remove these permanently:

      git rm --cached
      git commit -m "node removed"
      git push. 

- In the next step we can now go back to windows terminal (i prefer to work there so i keep SSH connected to the VM at the time), went to:

      cd devops-19-20-b-1090118/CA2/part2/demo
      
- And introduce the same commands used in part 1 before:

      ./gradlew clean build
      ./gradlew bootRun

- In the end if all goes well we just need go to:

    http://192.168.56.5:8080
    
- And verify that it is the same page we put data back when we did the exercice CA2-part2.
	   
**4- At the end of the part 1 of this assignment mark your repository with the tag ca3-part1** 

- At this stage I create a folder (in windows) inside the project called CA3, with a folder Part1 inside it, and put a temporary ReadMe there.

- After making all the previous steps with success I created a issue called "Create folder CA3 and folder Part1 (inside CA3) with the temporary ReadMe part3 there", commit and push it.

- In the end all was left to do is create a tag called ca3-part1 and push it to the repository and did it using the commands:

      git tag ca3-part1
      git push origin ca3-part1
 


     

 
 
 