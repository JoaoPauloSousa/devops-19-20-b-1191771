CA2, Part 2: DescriptionOfApp / Goals
===================

- The goal of Part 2 of this assignment is to use Vagrant to setup a virtual environment to execute the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2).

**1- You should use https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ as an initial solution**

- First we need to install Vagrant, ind the end of instalation we could run the following command to see if it was successful:

      vagrant -v

- If successful should appears "vagrant 2.2.7".  
- Next, I created a folder called "Part2" inside CA3 and clone the project in https://bitbucket.org/atb/vagrant-multi-spring-tut-demo/ to that folder.
- After that, i went to:

      cd devops-19-20-b-1191771/CA3/Part2/vagrant-multi-spring-tut-demo 

 - Next, I introduce the command:
 
       vagrant up

- This will create 2 VM, one called part2_db... and other called part2_web...
-With them running, we could went to the web brower and verify is they are working using this links:

      http://localhost:8080/basic-0.0.1-SNAPSHOT/
      http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/
      http://localhost:8080/basic-0.0.1-SNAPSHOT/h2-console
      http://192.168.33.10:8080/basic-0.0.1-SNAPSHOT/h2-console
 
- To test the connection we can use the String connection:
 
        jdbc:h2:tcp://192.168.33.11:9092/./jpadb 
            
- If they work, we confirm that vagrant is indeed working. 
   
**2- Study the Vagrantfile and see how it is used to create and provision 2 VMs:**
     
-  web: this VM is used to run tomcat and the spring boot basic application
-  db: this VM is used to execute the H2 server database      

**3- Copy this Vagrantfile to your repository (inside the folder for this assignment)**
  
- I made a copy of CA2/Part2 to CA3/Part2, change the name to "vagrantProject" and copy the Vagrantfile to:

      devops-19-20-b-1191771/CA3/Part2/vagrantProject/demo
      
- This way, the original CA2/Part2 is still unchanged.
- Ate this stage, i did a commit and push to send this changes.
      
**4- Update the Vagrantfile configuration so that it uses your own gradle version of the spring application** 

-  Here, I have some problems with the authentication (ask for credentials) so in order to avoid that the repository was made public, then I link the file to the repository by doing this changes in Vagrantfile:

       git clone https://JoaoPauloSousa@bitbucket.org/JoaoPauloSousa/devops-19-20-b-1191771.git
       cd devops-19-20-b-1191771/CA3/Part2/vagrantProject/demo
       chmod +x gradlew
       ./gradlew clean build
       sudo cp build/libs/demo-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

**5- Check https://bitbucket.org/atb/tut-basic-gradle to see the changes necessary so that the spring application uses the H2 server in the db VM. Replicate the changes in your own version of the spring application.**    
 
- Now it's the _easy part_, I say that because I personally found that initial steps a little confusing and had a really hard time to understand what I was supposed to do.
- All we need to do is replicate the changes demonstrated there and in the end run:

      vagrant up
      
- In:

      devops-19-20-b-1191771/CA3/Part2/vagrantProject/demo
        
- Now we can use the same links provided above (changing _basic_ to _demo_) and we can confirm it's all working.
- In my work it failed the first time because i put one line of code in the wrong place, so i need to correct it, push the changes, destroy the machines and run it again.
- Even after that the tables only show the headers, so after some researching in google I try to destroy the machines again, delete the folder _.vagrant_ inside the project, push it again and after that, all the links work like they should.

**5- At the end of the part 2 of this assignment mark your repository with the tag ca3-part2.**    
- In the end, after this report is made and pushed we need to mark the repository as ca3-part2 following the usual steps:

      git tag ca3-part2
      git push origin ca3-part2
           
- On a side note I must say I complete everything from the two parts but only complete the two readmes today and don't implement the alternative.
     

 
 
 