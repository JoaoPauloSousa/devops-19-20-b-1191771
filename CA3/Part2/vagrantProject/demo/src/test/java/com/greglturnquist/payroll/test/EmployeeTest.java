package com.greglturnquist.payroll.test;

import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EmployeeTest {

    /**
     * test for constructor
     */
    @Test
    @DisplayName("Test for Person constructor - Happy case")
    void constructorHappyCaseTest() {
        // ARRANGE
        Employee joao = new Employee("joao", "sousa", "switch", "student");

        // ACT
        // ASSERT
        assertTrue(joao instanceof Employee);
    }

    /**
     * Test for equals
     */
    @Test
    @DisplayName("Test for Equals")
    void equalsTest() {
        // ARRANGE
        Employee joao = new Employee("joao", "sousa", "switch", "student");
        Employee otherJoao = new Employee("joao", "sousa", "switch", "student");

        // ACT
        // ASSERT
        assertEquals(joao, otherJoao);
    }
}