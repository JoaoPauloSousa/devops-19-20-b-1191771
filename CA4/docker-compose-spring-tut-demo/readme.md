CA4: Goals / Requirements
===================

- The goal of this assignment is to use Docker to setup a containerized environment to execute your version of the gradle version of spring basic tutorial application

**1- You should produce a solution similar to the one of the part 2 of the previous CA but now using Docker instead of Vagrant.**

- In this step I train with the project provided by the teacher avaliable to download at: 

https://bitbucket.org/atb/docker-compose-spring-tut-demo/

- And clone it to CA4 folder in my project, then after installed docker toolbox spent some time play with it.

- Pro tip: Before make the _build_ and _up_ use:

      docker-machine ip default

-So we can see the IP to be used.

- I use this two links:

      192.168.99.100:8080/basic-0.0.1-SNAPSHOT/

      192.168.99.100:8080/basic-0.0.1-SNAPSHOT/h2-console

- Using the string connection:

  jdbc:h2:tcp://192.168.33.11:9092/./jpadb

- Where I could confirm everything work well

    
**2- You should use docker-compose to produce 2 services/containers:**      

**- web: this container is used to run Tomcat and the spring application**
      
**- db: this container is used to execute the H2 server database**

- Now we need to change the Dockerfile inside:

      devops-19-20-b-1191771/CA4/docker-compose-spring-tut-demo/web

- In order to make it work with my CA3/Part2/ and my repository. tha changes needed were:

      ...RUN git clone https://JoaoPauloSousa@bitbucket.org/JoaoPauloSousa/devops-19-20-b-1191771.git

      WORKDIR /tmp/build/devops-19-20-b-1191771/CA3/Part2/vagrantProject/demo

      RUN chmod u+x gradlew (firts I did it only with +x but the browser could only display the headers of the table) 

      RUN ./gradlew clean build

      RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/...

- Now we can build it, first go to were the .yml are:

      devops-19-20-b-1191771/CA4/docker-compose-spring-tut-demo/

- And introduce the following command:

      docker-compose build

- After that (it will take some time) we execute the container with:

      docker-compose up

- Now we can go to:

      192.168.99.100:8080/demo-0.0.1-SNAPSHOT/ (don't forget to change the name from basic to demo in our case)

      192.168.99.100:8080/demo-0.0.1-SNAPSHOT/h2-console (same here).

- Using the same connection as before:

      jdbc:h2:tcp://192.168.33.11:9092/./jpadb
      
- After that I did the first commit and push. Remember to always delete the .idea folder when clone project from another source, I spent a whole day around this because I forgot this.
      
**3- Publish the images (db and web) to Docker Hub (https://hub.docker.com)**

To do this task we need to take several steps.

- First register (if it is the first time) and log in in: 
 
      https://hub.docker.com/
      
- Then click on: 

      Create Repository
      
- After that we must choose a name, a description for our repository and click 

      Create
      
**The website part is now complete, now we must open the Docker QuickStart terminal and do the following:**

- In the command line enter your credentials when requested using the command:

      docker login
         
- Check the image ID using the command: 
   
      docker images
           
- Tag the wanted image with:

      docker tag <IMAGEID> joaos/devops-19-20-b-1191771:Ca4_web (or db instead of web, we must do both)
    
- Then we can push our image to the repository we created:
        
      docker push joaos/devops-19-20-b-1191771:Ca4_web (or db instead of web, we must do both)

- Remember that we must do this to both _web_ and _db_ images.

- After that I did a second commit and push with a preliminary version of this readme.

**4- Use a volume with the db container to get a copy of the database file by using the
     exec to run a shell in the container and copying the database file to the volume.**
     
- In order to do this I opened the Docker QuickStart terminal, then went to:

        /devops-19-20-b-1191771/CA4/docker-compose-spring-tut-demo
        
 - where the yml file is located and initiate the containers with:

        docker-compose up
        
- After that we must go to another terminal (IntelliJ for example) to the folder where the project is, in my case:

      /devops-19-20-b-1191771/CA4/docker-compose-spring-tut-demo 
 
- And put the following commands:

        docker-compose exec db bash
        cp /usr/src/app/jpadb.mv.db /usr/src/data
        
- With that commands we will execute a bash shell on the container and then make a copy of the jpadb-mv.db file to the data folder, if everything goes well the file will now appear on IntelliJ. 

- In the end before commit and push we must comment _/data/*.db_ line in -gitignore so this new file will be sent to the repository.

- After that I completed this readme, push it to the repository and mark it tag ca4.





     

 
 
 