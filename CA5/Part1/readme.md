CA5-Part1: Goals / Requirements
===================

- The goal of the Part 1 of this assignment is to practice with Jenkins using the "gradle basic demo" project that should already be present in the student’s individual repository.

**1- The goal is to create a very simple pipeline (similar to the example from the lectures).**

- The very first step to take is to install Jenkins. In order to do that we need to download a _.war_ file called _jenkins_ which is available at: 

      https://www.jenkins.io/doc/book/installing/#war-file

- Now we need to open a terminal, go to the same directory where _jenkis.war_ file are and run the following command:

      java -jar jenkins.war

- After some time will appear a long password which ideally should be copy to a notepad file for example. 

- Now we can open a browser and go to:

      http://localhost:8080

- Use the same password that appears in the installation before, and then create an account in Jenkins using the necessary plug-ins:

- In my account I choose João Paulo Matos e Sousa as username and joaosousa as userId.

- Next step is to introduce bitbucket credentials (we will need them after), in order do to that we need to go to _credentials_ and choose on username, iD and description. The required password is the bitbucket one. 

- I opted joaopaulosousa-bitbucket as my id for this credential.
    
**2- You should define the following stages in your pipeline:**      

**- Checkout. To checkout the code from the repository**
      
**- Assemble. Compiles and Produces the archive files with the application. Do not use the build task of gradle (because it also executes the tests)!**

**- Test. Executes the Unit Tests and publish in Jenkins the Test results. See the junit step for further information on how to archive/publish test results. Do not forget to add some unit tests to the project (maybe you already have done it).**

**- Archive. Archives in Jenkins the archive files (generated during Assemble)**

- Now we need to create a pipeline. In order to do that we should go to _New item_, put an adequate name and then click on _pipeline_.

- Now we need to choose _Pipeline script_ and introduce the right script to do what I need. In my case in order to met the requirements i use the following script:

      pipeline {
          agent any
          stages {
              stage('Checkout') {
                  steps {
                      echo 'Checking out...'
                      git credentialsId: 'joaopaulosousa-bitbucket', url: 'https://JoaoPauloSousa@bitbucket.org/JoaoPauloSousa/devops-19-20-b-1191771.git'
                  }
              }
              stage('Assemble') {
                  steps {
                      echo 'Building...'
                      bat 'cd CA2/Part1/gradle_basic_demo & gradlew clean assemble'
                  }
              }
              stage ('Test'){
                  steps {
                   echo 'Testing...'
                   bat 'cd CA2/Part1/gradle_basic_demo & gradlew test'
                   junit 'CA2/Part1/gradle_basic_demo/build/test-results/test/*.xml'
                  }
              }
              stage('Archive') {
                  steps {
                      echo 'Archiving...'
                      archiveArtifacts 'CA2/Part1/gradle_basic_demo/build/distributions/*'
                  }
              }
          }
      }

- After confirm that this script work I did a commit with _Jenkinsfile_ on it. This file contains the script used above:

- But we can also create a jenkinsfile through the repository. In order to do that we again should go to _New item_, put an adequate name and then click on _pipeline_.

- Now select _Pipeline script from SCM_, change _SCM_ to _Git_, put our bitbucket link in _repository url_, use the credentials that we stored in the very beginning in the field _credentials_, and choose the right path (where _jenkinsfile have been stored), in my case

      ca5/Part1/Jenkinsfile  

- Then click _save/apply_, and in the next page click _buildNow_. After success were confirmed all we need to do were commit, push and tag with ca5-part1 which i did same way as previous assignments. I opted for only send the readme file in the end with part2.






     

 
 
 