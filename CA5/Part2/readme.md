CA5-Part2: Goals / Requirements
===================

- The goal of the Part 2 of this assignment is to create a pipeline in Jenkins to build the tutorial spring boot application, gradle "basic" version (developed in CA2, Part2).

**1- You should define the following stages in your pipeline:**

**- Checkout. To checkout the code from the repository**
      
**- Assemble. Compiles and Produces the archive files with the application. Do not use the build task of gradle (because it also executes the tests)!**

**- Test. Executes the Unit Tests and publish in Jenkins the Test results. See the junit step for further information on how to archive/publish test results. Do not forget to add some unit tests to the project (maybe you already have done it).**

**- Javadoc. Generates the javadoc of the project and publish it in Jenkins. See the publishHTML step for further information on how to archive/publish html reports.**

**- Archive. Archives in Jenkins the archive files (generated during Assemble, i.e., the war file)**

**- Publish Image. Generate a docker image with Tomcat and the war file and publish it in the Docker Hub. See https://jenkins.io/doc/book/pipeline/docker/, section Building Containers. See also the next slide for an example. In the example, docker.build will build an image from a Dockerfile in the same folder as the Jenkinsfile. The tag for the image will be the job build number of Jenkins.**

- I started by create two very simple tests, one for constructor and one for equals (because i don't have any) in class _Employee_.

- After that I started by created another pipeline, this time using ca2-part2. I try same script as before (making necessary changes for that project) and try adding the new necessary stages.

- After some frustrating "try and error" I found this script to be working which i believe, met all the requirements:

        pipeline {
                agent any
        
                stages {
                    stage('Checkout') {
                        steps {
                            echo 'Checking out...'
                            git credentialsId: '1191771-bitbucket', url: 'https://JoaoPauloSousa@bitbucket.org/JoaoPauloSousa/devops-19-20-b-1191771.git'
                        }
                    }
        
                    stage('Assemble') {
                        steps {
                            echo 'Building...'
                            bat 'cd CA3/Part2/vagrantProject/demo & gradlew clean assemble'
                        }
                    }
        
                    stage ('Test'){
                        steps {
                            echo 'Testing...'
                            bat 'cd CA3/Part2/vagrantProject/demo & gradlew test'
                            junit 'CA3/Part2/vagrantProject/demo/build/test-results/test/*.xml'
                        }
                    }
        
                    stage('Archive') {
                        steps {
                            echo 'Archiving...'
                            archiveArtifacts 'CA3/Part2/vagrantProject/demo/build/libs/*'
                        }
                    }
                    
                    stage ('Javadoc'){
                     steps {
                        echo 'Publishing...'
                        bat 'cd CA3/Part2/vagrantProject/demo/ & gradlew javadoc'
        
                         publishHTML([
                                     reportName: 'Javadoc',
                                     reportDir: 'CA3/Part2/vagrantProject/demo/build/docs/javadoc/',
                                     reportFiles: 'index.html',
                                      keepAll: false,
                                     alwaysLinkToLastBuild: false,
                                     allowMissing: false
                                      ])
                     }
                  }
                  
                  stage ('Publish Image') {
                        steps {
                            echo 'Publishing image...'
                             script {
                         docker.withRegistry('https://index.docker.io/v1/', 'joaosousa-docker') {
                                                def customImage = 
                                                docker.build("1191771/devops-19-20-b-1191771:${env.BUILD_ID}", "CA5/Part2")
                                                customImage.push()
                                            }
                             }
                        }
                    }
                }
            }

- One very important thing to remember is that capital letters matters and one capital letter (ou vice versa) wrong is enough to make the build will fail.

- For the sake of don't wasting precious time I use same web _dockerfile used in CA4, and this also explains why i used ca3-part2. I initially start (and push) tests for Ca2-part but then i realize that using Ca3-part2 were faster so i put (and push) my tests there as well.

- At last is also important to refer that we need to put docker credentials on jenkins as well. The process is the same as described on part1 for bitbucket.

- If everything went well a success message will appear, and the build is now done.

**- Now I will explain briefly what each stage do** 

- Checkout Stage: We just need to put our git credentials and url.

- Assemble Stage: Making the build and changing directories to where the initialization file is, after that the task clean is done and finally assemble all files of the project.

- Test Stage: As the name suggests is where the tests are executed after we change to that folder. In the end the junit line will allow to publish the results of the tests.

- Javadoc Stage: Generates the javadoc file and publish it on jenkins. In order to see the information produced here we must first install _publisher html_ plugin on jenkins.

- Archive Stage: This step will archive the selected generated files in the assemble stage.

- Publish Image Stage: Generate a docker image with the _war_ file and send it to DockerHub.

**Now we can start to create a pipeline trough the _repository_ as we have done in part1 of this assigment**

- All the steps and fields are exactly the same as part1 except this time the _Script Path_ is Ca5/Part2/Jenkinsfile.
    
- If everything went well a success message will appear, and the build is now done and the assigment complete.

- All is left to do is commit, push and tag it with ca5-part2. I did commit it and push, but wait to tag it with ca5-part2 until the two readmes for this assigment were finished. 

    

     
      





     

 
 
 